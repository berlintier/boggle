import requests
from flask import Flask, request
import ApiConfig
from flask_cors import CORS
import json

app = Flask(__name__)
CORS(app)


@app.route("/valid")
def valid():
	word_to_search = request.args.get('word')
	result = "missinginput"
	if not word_to_search:
		return json.dumps({'result': result})

	url = ApiConfig.URL_PREFIX  + word_to_search
	response = requests.get(url, headers = ApiConfig.URL_HEADER)
	result = "valid" if response.status_code == 200 else "invalid"
	return json.dumps({'result': result})	

if __name__ == '__main__':
    app.run(debug=True)
