import * as React from 'react';
import './App.css';
import { apiUrlPrefix, data, lowerBound, upperBound } from './Consts';

interface IComponentProp {
  result?: string
  word?: string;
  renderSquare?: boolean;
}

class App extends React.Component<IComponentProp> { 
  public state = {
    renderSquare: true, 
    result: "",  
    word: "",
  };

  constructor(props: any) {
    super(props);
  }

  public componentDidMount() {  
    this.setState({ 
      renderSquare: this.state.renderSquare,
      result: ""
    }); 
  }  
 
  public  handleWordChange(element: any) {
      this.setState({ word: element.target.value });
  }

  public render() {  
    return (
      <div>
        {this.state.renderSquare ? this.getBoggleSquare() : "" }
        <br/><br/><br/>
        {this.getForm()}
        <div>{this.state.result}</div>
      </div>
    );
  }

  private getBoggleSquare() {
    return (
      <div className="game">
          <div className="board-row">
            {this.renderSquare(0)}
            {this.renderSquare(1)}
            {this.renderSquare(2)}
            {this.renderSquare(3)}
          </div>
          <div className="board-row">
            {this.renderSquare(4)}
            {this.renderSquare(5)}
            {this.renderSquare(6)}
            {this.renderSquare(7)}
          </div>
          <div className="board-row">
            {this.renderSquare(8)}
            {this.renderSquare(9)}
            {this.renderSquare(10)}
            {this.renderSquare(11)}
          </div>
          <div className="board-row">
            {this.renderSquare(12)}
            {this.renderSquare(13)}
            {this.renderSquare(14)}
            {this.renderSquare(15)}
          </div>
        </div>
    );
  }

  private getForm() {
    return (
      <form role="form" onSubmit={this.isWordValid}>
        <input
            type="text"
            name="word"
            ref={node => (this.state.word == null ? "noinput" : node)}
          />
          <button type="submit">Submit</button>
        </form>
    );
  }


  private isWordValid() {
    this.state.renderSquare = false;
    const endpoint = apiUrlPrefix.concat(this.state.word);
      fetch(endpoint).then((response) => {
        response.json().then((data1) => {
          this.setState({result: data1.result});
        });
      });
  }

  private renderSquare(i: number) {
    return (
      <div className="square">
        {this.getLetter(i)}
      </div>
    );
  }

  private getLetter(i: number) {
    return data[i][Math.floor(Math.random() * upperBound) + lowerBound];
  }
}


export default App;
