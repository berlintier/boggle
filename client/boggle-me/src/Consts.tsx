export const lowerBound = 0;
export const upperBound = 5;

export const apiUrlPrefix = "http://localhost:5000/valid?word=";

export const data: string[] = new Array();
data.push("AAEEGN");
data.push("ABBJOO");
data.push("ACHOPS");
data.push("AFFKPS");

data.push("AOOTTW");
data.push("CIMOTU");
data.push("DEILRX");
data.push("DELRVY");

data.push("DISTTY");
data.push("EEGHNW");
data.push("EEINSU");
data.push("EHRTVW");

data.push("EIOSST");
data.push("ELRTTY");
data.push("HIMNU7");
data.push("HLNNRZ");